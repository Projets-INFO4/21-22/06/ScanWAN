import { Alert } from "react-native";

import { STRINGS, FILES } from "../assets/strings";

export function storeLogTTN(APIKey) {
  // require the module
  const RNFS = require("react-native-fs");

  // create a path you want to write to
  // :warning: on iOS, you cannot write into `RNFS.MainBundlePath`,
  // but `RNFS.DocumentDirectoryPath` exists on both platforms and is writable
  const path = RNFS.DocumentDirectoryPath + FILES.filename_ttn;

  return (
    // write the file
    RNFS.writeFile(
      path,
      '{"name" : "The Things Network" , "APIkey" : "' + APIKey + '" }',
      "utf8"
    )
      .then(() => {})
      .catch((err) => {
        Alert.alert(STRINGS.error_title, STRINGS.could_not_register_info);
      })
  );
}

export function storeLogChirpstack(login, password) {
  // require the module
  const RNFS = require("react-native-fs");

  // create a path you want to write to
  // :warning: on iOS, you cannot write into `RNFS.MainBundlePath`,
  // but `RNFS.DocumentDirectoryPath` exists on both platforms and is writable
  const path = RNFS.DocumentDirectoryPath + FILES.filename_campus_iot;

  return (
    // write the file
    RNFS.writeFile(
      path,
      '{"name" : "Campus IoT" , "login" : "' +
        login +
        '","password" : "' +
        password +
        '" }',
      "utf8"
    )
      .then((success) => {})
      .catch((err) => {
        Alert.alert(STRINGS.error_title, STRINGS.could_not_register_info);
      })
  );
}

export function storeLogHelium(APIKey) {
  // require the module
  const RNFS = require("react-native-fs");

  // create a path you want to write to
  // :warning: on iOS, you cannot write into `RNFS.MainBundlePath`,
  // but `RNFS.DocumentDirectoryPath` exists on both platforms and is writable
  const path = RNFS.DocumentDirectoryPath + FILES.filename_helium;

  return (
    // write the file
    RNFS.writeFile(
      path,
      '{"name" : "Helium" , "APIkey" : "' + APIKey + '" }',
      "utf8"
    )
      .then(() => {})
      .catch((err) => {
        Alert.alert(STRINGS.error_title, STRINGS.could_not_register_info);
      })
  );
}

export function getLogTTN() {
  const RNFS = require("react-native-fs");

  // create a path you want to write to
  // :warning: on iOS, you cannot write into `RNFS.MainBundlePath`,
  // but `RNFS.DocumentDirectoryPath` exists on both platforms and is writable
  const path = RNFS.DocumentDirectoryPath + FILES.filename_ttn;

  return RNFS.readFile(path, "utf8")
    .then((contents) => {
      // log the file contents
      return JSON.parse(contents);
    })
    .catch((err) => {
      return undefined;
    });
}

export async function getLogChirpstack() {
  const RNFS = require("react-native-fs");

  // create a path you want to write to
  // :warning: on iOS, you cannot write into `RNFS.MainBundlePath`,
  // but `RNFS.DocumentDirectoryPath` exists on both platforms and is writable
  const path = RNFS.DocumentDirectoryPath + FILES.filename_campus_iot;
  const res = await RNFS.readFile(path, "utf8")
    .then((contents) => {
      // log the file contents
      const data = JSON.parse(contents);
      return data;
    })
    .catch((err) => {
      return undefined;
    });

  return res;
}

export async function getLogHelium() {
  const RNFS = require("react-native-fs");

  // create a path you want to write to
  // :warning: on iOS, you cannot write into `RNFS.MainBundlePath`,
  // but `RNFS.DocumentDirectoryPath` exists on both platforms and is writable
  const path = RNFS.DocumentDirectoryPath + FILES.filename_helium;
  const res = await RNFS.readFile(path, "utf8")
    .then((contents) => {
      // log the file contents
      const data = JSON.parse(contents);
      return data;
    })
    .catch((err) => {
      return undefined;
    });

  return res;
}
