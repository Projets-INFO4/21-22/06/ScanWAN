import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  DeviceEventEmitter,
} from "react-native";
import { connect } from "react-redux";

import { NETWORKS, EVENTS, SCREENS } from "../assets/strings";

class ApplicationItem extends React.Component {
  constructor(props) {
    super(props);
  }

  selectApplication(app) {
    switch (this.props.selectedNetwork) {
      case NETWORKS.campus_iot:
        this.props.dispatch({
          type: EVENTS.choose_app,
          value: { name: app.name, id: app.id },
        });
        break;
      case NETWORKS.ttn:
        this.props.dispatch({
          type: EVENTS.choose_app,
          value: { name: app.application_id, id: app.application_id },
        });
        break;
      case NETWORKS.helium:
        this.props.dispatch({
          type: EVENTS.choose_app,
          value: { name: app.name, id: app.name },
        });
        break;
      default:
        break;
    }

    DeviceEventEmitter.emit(EVENTS.selected_application);
    this.props.navigator.navigate(SCREENS.Scanner, {
      selectedNetwork: this.props.selectedNetwork,
      selectedApp: app,
    });
  }

  ttnItem(application) {
    return (
      <TouchableHighlight
        underlayColor="#BFBFBF"
        onPress={() => this.selectApplication(application)}
      >
        <View style={styles.main_container}>
          <View style={styles.title_container}>
            <Text style={styles.name}>{application.application_id}</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }

  chirpstackItem(application) {
    return (
      <TouchableHighlight
        underlayColor="#BFBFBF"
        onPress={() => this.selectApplication(application)}
      >
        <View style={styles.main_container}>
          <View style={styles.title_container}>
            <Text style={styles.name}>{application.name}</Text>
          </View>
          <View style={styles.details_container}>
            <Text style={styles.description}>{application.description}</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }

  heliumItem(application) {
    return (
      <TouchableHighlight
        underlayColor="#BFBFBF"
        onPress={() => {
          this.selectApplication(application);
        }}
      >
        <View style={styles.main_container}>
          <View style={styles.title_container}>
            <Text style={styles.name}>{application.name}</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }

  render() {
    const application = this.props.application;
    switch (this.props.selectedNetwork) {
      case NETWORKS.campus_iot:
        return this.chirpstackItem(application);
      case NETWORKS.ttn:
        return this.ttnItem(application);
      case NETWORKS.helium:
        return this.heliumItem(application);
      default:
        break;
    }
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
  },

  title_container: {
    flex: 1,
  },

  details_container: {
    flex: 2,
    flexDirection: "row",
  },

  name: {
    fontSize: 24,
    fontWeight: "500",
    marginBottom: 10,
    marginTop: 10,
    marginLeft: 5,
  },

  description: {
    marginLeft: 5,
  },
});

const mapStateToProps = (state) => {
  return { selectedNetwork: state.selectedNetwork };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => {
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationItem);
