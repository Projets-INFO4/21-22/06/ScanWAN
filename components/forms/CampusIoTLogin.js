import React from "react";
import {
  SafeAreaView,
  View,
  StyleSheet,
  DeviceEventEmitter,
  ToastAndroid,
} from "react-native";
import { Text, Icon, Card, Button, Input } from "@ui-kitten/components";
import { useForm, Controller } from "react-hook-form";
import { connect } from "react-redux";
import { storeLogChirpstack } from "../Storage";

import { STRINGS, EVENTS } from "../../assets/strings";

const checkIcon = (props) => <Icon {...props} name="done-all-outline" />;

function CampusIoTLogin(props) {
  const { control, handleSubmit, errors } = useForm();

  const onSubmit = async (data) => {
    storeLogChirpstack(data.login, data.passwd);
    DeviceEventEmitter.emit(EVENTS.e_clear);
    props.dispatch({ type: EVENTS.clear, value: "" });
    ToastAndroid.show(STRINGS.network_added, ToastAndroid.SHORT);
    props.navigation.popToTop();
  };

  return (
    <SafeAreaView style={styles.main_view}>
      <Text style={styles.title}>{STRINGS.title_campus_iot_login}</Text>

      <Card style={styles.card} status="primary" disabled={true}>
        <View style={styles.view_form}>
          <Controller
            control={control}
            render={({ onChange, onBlur, value }) => (
              <>
                <Text category="p1">{STRINGS.login}</Text>
                <Input
                  style={styles.input}
                  onBlur={onBlur}
                  onChangeText={(value) => onChange(value)}
                  value={value}
                />
              </>
            )}
            name="login"
            rules={{ required: true }}
            defaultValue=""
          />

          {errors.login && (
            <Card status="danger">
              <Text>{STRINGS.login_needed}</Text>
            </Card>
          )}

          <Controller
            control={control}
            render={({ onChange, onBlur, value }) => (
              <>
                <Text category="p1">{STRINGS.password}</Text>
                <Input
                  style={styles.input}
                  onBlur={onBlur}
                  onChangeText={(value) => onChange(value)}
                  value={value}
                  secureTextEntry={true}
                />
              </>
            )}
            name="passwd"
            rules={{ required: true }}
            defaultValue=""
          />

          {errors.passwd && (
            <Card status="danger">
              <Text>{STRINGS.password_needed}</Text>
            </Card>
          )}
        </View>
      </Card>

      <Button
        style={styles.button}
        accessoryLeft={checkIcon}
        onPress={handleSubmit(onSubmit)}
      >
        {STRINGS.submit}
      </Button>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  main_view: {
    paddingTop: "2%",
    flex: 1,
  },
  field: {
    justifyContent: "flex-end",
    flexDirection: "row",
    flex: 1,
  },
  input: {
    backgroundColor: "white",
    height: 40,
    borderWidth: 0.1,
    borderRadius: 4,
  },
  card: {
    marginLeft: "2%",
    marginRight: "2%",
  },
  button: {
    marginLeft: "2%",
    marginRight: "2%",
    marginTop: "2%",
  },
  title: {
    fontSize: 36,
    fontWeight: "bold",
    textAlign: "center",
    margin: 15,
  },
});

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => {
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CampusIoTLogin);
