import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
} from "react-native";
import { connect } from "react-redux";
import { DeviceEventEmitter } from "react-native";
import { getToken } from "../api/CampusIoT";

import { NETWORKS, EVENTS, SCREENS } from "../assets/strings";

class NetworkItem extends React.Component {
  constructor(props) {
    super(props);
  }

  async selectNetwork(network) {
    this.props.dispatch({ type: EVENTS.choose_network, value: network.name });
    let token;

    switch (network.name) {
      case NETWORKS.campus_iot:
        let login, password;
        login = network.login;
        password = network.password;
        this.props.dispatch({
          type: EVENTS.add_log,
          value: { login: login, password: password },
        });
        token = await getToken(login, password);
        break;
      case NETWORKS.ttn:
        token = network.APIkey;
        break;
      case NETWORKS.helium:
        token = network.APIkey;
        break;
      default:
        break;
    }

    this.props.dispatch({ type: EVENTS.add_jwt, value: token });
    this.props.dispatch({ type: EVENTS.remove_app, value: "" });

    DeviceEventEmitter.emit(EVENTS.selected_network);

    switch (network.name) {
      case NETWORKS.campus_iot:
        this.props.navigator.navigate(SCREENS.ApplicationSelect);
        break;
      case NETWORKS.ttn:
        this.props.navigator.navigate(SCREENS.ApplicationSelect);
        break;
      case NETWORKS.helium:
        this.props.navigator.navigate(SCREENS.Scanner, {
          selectedNetwork: network.name,
        });
        break;
      default:
        break;
    }
  }

  render() {
    const network = this.props.network;
    return (
      <TouchableHighlight
        underlayColor="#BFBFBF"
        onPress={() => this.selectNetwork(network)}
      >
        <View style={styles.main_container}>
          <View style={styles.image_container}>
            <Image style={styles.image} source={network.image} />
          </View>

          <View style={{ borderRightWidth: 1, borderRightColor: "grey" }} />

          <View style={styles.details_container}>
            <Text style={styles.name}>{network.name}</Text>
            <Text style={styles.login}>{network.login}</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    flexDirection: "row",
    height: 100,
    margin: 10,
  },

  image_container: {
    width: 120,
  },

  image: {
    flex: 1,
    resizeMode: "contain",
    width: 110,
    height: undefined,
    margin: 5,
  },

  details_container: {
    flex: 1,
    justifyContent: "center",
  },

  name: {
    fontSize: 24,
    fontWeight: "500",
    margin: 10,
  },

  login: {
    fontSize: 16,
    margin: 10,
  },
});

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => {
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NetworkItem);
