import React from "react";
import { Text, Button } from "@ui-kitten/components";
import { SafeAreaView, View, StyleSheet, ToastAndroid } from "react-native";
import { Picker } from "@react-native-picker/picker";
import jsyaml from "js-yaml";
import Clipboard from "@react-native-community/clipboard";

import { STRINGS, API, SCREENS } from "../assets/strings";


class FunctionSelect extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      vendors: null,  vendor: "default",
      devices: null,  device: "default",
      versions: null, version: "default",
      profiles: null, profile: "default",

      nextButton: null,
      functionCode: null,
      loading: true
    };
  }


  updateVendor = (itemValue, itemIndex) => {
    this.setState({
      vendor: itemValue,
      devices: null,  device: "default",
      versions: null, version: "default",
      profiles: null, profile: "default",
      nextButton: null, functionCode: null
    });
    if (itemValue != "default")
      this.loadDevices(itemValue);
  };

  updateDevice = (itemValue, itemIndex) => {
    this.setState({
      device: itemValue,
      versions: null, version: "default",
      profiles: null, profile: "default",
      nextButton: null, functionCode: null
    });
    if (itemValue != "default")
      this.loadVersions(this.state.vendor, itemValue);
  };

  updateVersion = (itemValue, itemIndex) => {
    this.setState({
      version: itemValue,
      profiles: null, profile: "default",
      nextButton: null, functionCode: null
    });
    if (itemValue != "default")
      this.loadProfiles(itemIndex - 1);
  };

  updateProfile = (itemValue, itemIndex) => {
    this.forceUpdateProfile(itemValue, itemIndex, this.state.profiles);
  }

  forceUpdateProfile = (itemValue, itemIndex, profiles) => {
    this.setState({
      profile: itemValue,
      nextButton: null, functionCode: null
    });
    if (itemValue != "default")
      this.loadFunction(this.state.vendor, profiles[itemValue].codec);
  };

  async loadVendors() {
    fetch(API.functions_base_url + "/" + API.functions_index_path)
      .then((response) => response.text())
      .then((data) => {
        let vendors = jsyaml.load(data).vendors;
        fetch(
          API.functions_tree_url,
          {
            method: "GET",
            headers: new Headers([
              ["Accept", "application/vnd.github.v3+json"]
            ])
          })
          .then((response) => response.json())
          .then((json) => {
            // Filtrage des vendors, car ils n'ont pas tous de fonctions
            let entries = json.tree;
            let validVendors = [];
            let lastValidVendor = null;
            for (let i = 0; i < entries.length; i++) {
              let path = entries[i].path;
              let separatorIndex = path.indexOf("/");
              let vendor = path.substring(0, separatorIndex);
              if (vendor != lastValidVendor && separatorIndex != -1 && path.endsWith(".js")) {
                validVendors.push(vendor);
                lastValidVendor = vendor;
              }
            }

            for (let i = 0; i < vendors.length; i++) {
              if (validVendors.indexOf(vendors[i].id) == -1) {
                delete vendors[i];
              }
            }

            //delete vendors[0]; Not needed as we filter entries
            this.setState({
              vendors: vendors,
              loading: false
            });
            if (vendors.length == 1) {
              this.updateVendor(vendors[0].id, 0);
            }
          });
      });
  }

  async loadDevices(vendorID) {
    fetch(API.functions_base_url + "/" + vendorID + "/" + API.functions_index_path)
      .then((response) => response.text())
      .then((data) => {
        let devices = jsyaml.load(data).endDevices;
        this.setState({ devices: devices });
        if (devices.length == 1) {
          this.updateDevice(devices[0], 0);
        }
      });
  }

  async loadVersions(vendorID, device) {
    fetch(API.functions_base_url + "/" + vendorID + "/" + device + ".yaml")
      .then((response) => response.text())
      .then((data) => {
        let versions = jsyaml.load(data).firmwareVersions;
        this.setState({ versions: versions });
        if (versions.length == 1) {
          this.updateVersion(versions[0].version, 1);
        }
      });
  }

  loadProfiles(versionIndex) {
    let profiles = this.state.versions[versionIndex].profiles;
    this.setState({ profiles: profiles });

    let keys = Object.keys(profiles);
    if (keys.length == 1) {
      this.forceUpdateProfile(keys[0], 0, profiles);
    }
  }

  async loadFunction(vendorID, codecName) {
    fetch(API.functions_base_url + "/" + vendorID + "/" + codecName + ".yaml")
      .then((response) => response.text())
      .then((data) => {
        fetch(API.functions_base_url + "/" + vendorID + "/" + jsyaml.load(data).uplinkDecoder.fileName)
          .then((response) => response.text())
          .then((data) => {
            this.setState({ functionCode: data });
          });
      });
  }



  getVendorsPicker() {
    if (this.state.vendors == null) {
      return null;
    }

    return (
      <Picker
        //mode={'dropdown'}
        selectedValue={this.state.vendor}
        onValueChange={this.updateVendor}>
        <Picker.Item label={STRINGS.select_vendor} value={"default"} key={"default"} />
        {
          this.state.vendors.map(vendor =>
            <Picker.Item label={vendor.name} value={vendor.id} key={vendor.id} />
          )
        }
      </Picker>
    );
  }

  getDevicesPicker() {
    if (this.state.devices == null) {
      return null;
    }

    return (
      <Picker
        //mode={'dropdown'}
        selectedValue={this.state.device}
        onValueChange={this.updateDevice}>
        <Picker.Item label={STRINGS.select_device} value={"default"} key={"default"} />
        {
          this.state.devices.map(device =>
            <Picker.Item label={device} value={device} key={device} />
          )
        }
      </Picker>
    );
  }

  getVersionsPicker() {
    if (this.state.versions == null) {
      return null;
    }

    return (
      <Picker
        //mode={'dropdown'}
        selectedValue={this.state.version}
        onValueChange={this.updateVersion}>
        <Picker.Item label={STRINGS.select_version} value={"default"} key={"default"} />
        {
          this.state.versions.map(version =>
            <Picker.Item label={version.version} value={version.version} key={version.version} />
          )
        }
      </Picker>
    );
  }

  getProfilesPicker() {
    if (this.state.profiles == null) {
      return null;
    }

    return (
      <Picker
        //mode={'dropdown'}
        selectedValue={this.state.profile}
        onValueChange={this.updateProfile}>
        <Picker.Item label={STRINGS.select_profile} value={"default"} key={"default"} />
        {
          Object.entries(this.state.profiles).map(([profileName, profile]) =>
            <Picker.Item label={profileName} value={profileName} key={profileName} />
          )
        }
      </Picker>
    );
  }

  functionButtonPress = () => {
    alert(this.state.functionCode);
    this.confirmCopy();
  };

  finishButtonPress = () => {
    this.props.navigation.navigate(SCREENS.Home);
  };


  getButton(text) {
    if(text === STRINGS.button_display_function){
      return (
        <Button
          onPress={this.functionButtonPress}
          disabled={this.state.functionCode == null}
          style={styles.button}
        >
          {text}
        </Button>
      );
    }else if(text === STRINGS.button_finish){
      return (
        <Button
          onPress={this.finishButtonPress}
          disabled={this.state.functionCode == null}
          style={styles.button}
        >
          {text}
        </Button>
      );
    }

  }


  confirmCopy() {
    ToastAndroid.show(STRINGS.copied_clipboard, 3);
    Clipboard.setString(this.myFunction);
  }

  displayFunction() {
    return (
      <Text
        style={{
          color: "red",
          fontSize: 14,
          fontFamily: "Arial",
          textAlign: "center",
          marginTop: 3,
          marginLeft: 25,
          marginBottom: 17,
        }}
      >
        {this.state.functionCode}
      </Text>
    );
  }

  getLoadingText() {
    return this.state.loading ? (
      <Text>
        {STRINGS.loading_vendors}
      </Text>
    ) : null;
  }



  async componentDidMount() {
    this.loadVendors();
  }

  render() {
    return (
      <SafeAreaView style={styles.safe}>
        <View>
          <Text style={styles.title}>{STRINGS.title_function_selection}</Text>
          <Text style={styles.instruction}>
            {STRINGS.select_encoding_matching_device}
          </Text>
        </View>

        <View>
          {this.getLoadingText()}
          {this.getVendorsPicker()}
          {this.getDevicesPicker()}
          {this.getVersionsPicker()}
          {this.getProfilesPicker()}
        </View>

        <View style={styles.buttons_view}>
          {this.getButton(STRINGS.button_display_function)}
          {this.getButton(STRINGS.button_finish)}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safe: {
    flex: 1,
  },

  title: {
    fontSize: 36,
    fontWeight: "bold",
    textAlign: "center",
    margin: 15,
  },

  instruction: {
    textAlign: "center",
    fontSize: 16,
    marginBottom: 20,
    borderBottomColor: "black",
    borderBottomWidth: 1,
  },

  buttons_view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },

  button: {
    width: 100,
    margin: 20,
    textAlign: "center",
  },
});

export default FunctionSelect;
