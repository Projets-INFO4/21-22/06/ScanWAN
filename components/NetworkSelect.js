import { Text, Button, ButtonGroup } from "@ui-kitten/components";
import React from "react";
import { SafeAreaView, View, StyleSheet, FlatList } from "react-native";
import NetworkItem from "./NetworkItem";
import { getLogChirpstack, getLogTTN, getLogHelium } from "./Storage";

import { STRINGS, SCREENS } from "../assets/strings";

class NetworkSelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      networks: [],
    };
  }

  componentDidMount() {
    this._getLogs();
  }

  _getLogs() {
    getLogChirpstack()
      .then((data) => {
        if (data !== undefined) {
          data = {
            ...data,
            image: require("../assets/logo_campus_iot.png"),
            id: "0",
          };
          let nets = [data];
          this.setState({ networks: nets });
        }
      })
      .catch((error) => alert(error));
    getLogTTN()
      .then((data) => {
        if (data !== undefined) {
          data = {
            ...data,
            image: require("../assets/logo_ttn.png"),
            id: "1",
          };
          let nets = [...this.state.networks, data];
          this.setState({ networks: nets });
        }
      })
      .catch((error) => alert(error));
    getLogHelium()
      .then((data) => {
        if (data !== undefined) {
          data = {
            ...data,
            image: require("../assets/logo_helium.png"),
            id: "2",
          };
          let nets = [...this.state.networks, data];
          this.setState({ networks: nets });
        }
      })
      .catch((error) => alert(error));
  }

  _addChirpstackAccount = () => {
    this.props.navigation.navigate(SCREENS.CampusIoTLogin);
  };

  _addTTNAccount = () => {
    this.props.navigation.navigate(SCREENS.TTNLogin);
  };

  _addHeliumAccount = () => {
    this.props.navigation.navigate(SCREENS.HeliumLogin);
  };

  render() {
    return (
      <SafeAreaView style={styles.safe}>
        <View>
          <Text style={styles.title}>{STRINGS.title_select_network}</Text>
          <Text style={styles.instruction}>{STRINGS.select_network}</Text>
        </View>
        <FlatList
          data={this.state.networks}
          renderItem={(item) => (
            <NetworkItem
              network={item.item}
              navigator={this.props.navigation}
            />
          )}
          keyExtractor={(item) => item.id}
          ItemSeparatorComponent={renderSeparator}
        />

        <View style={styles.buttons_view}>
          <Text appearance="hint">{STRINGS.add_account}</Text>
          <ButtonGroup status="info">
            <Button onPress={this._addChirpstackAccount}>
              {STRINGS.campus_iot}
            </Button>
            <Button onPress={this._addTTNAccount}>{STRINGS.ttn}</Button>
            <Button onPress={this._addHeliumAccount}>{STRINGS.helium}</Button>
          </ButtonGroup>
        </View>
      </SafeAreaView>
    );
  }
}

const renderSeparator = () => {
  return (
    <View
      style={{
        backgroundColor: "black",
        height: 1,
        marginLeft: 10,
        marginRight: 10,
      }}
    />
  );
};

const styles = StyleSheet.create({
  safe: {
    flex: 1,
  },
  buttons_view: {
    flex: 1,
    alignItems: "center",
  },
  title: {
    fontSize: 36,
    fontWeight: "bold",
    textAlign: "center",
    margin: 15,
  },

  instruction: {
    textAlign: "center",
    fontSize: 16,
    marginBottom: 20,
    borderBottomColor: "black",
    borderBottomWidth: 1,
  },
});

export default NetworkSelect;
