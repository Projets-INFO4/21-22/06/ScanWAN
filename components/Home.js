import React from "react";
import { Text, Button } from "@ui-kitten/components";
import { SafeAreaView, StyleSheet, Image } from "react-native";

import { STRINGS, SCREENS } from "../assets/strings";

class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <SafeAreaView style={styles.safe}>
        <Image style={styles.logo} source={require("../assets/logo.png")} />
        <Text style={styles.instruction}>{STRINGS.welcome}</Text>
        <Button
          style={styles.button}
          onPress={() => {
            this.props.navigation.navigate(SCREENS.NetworkSelect);
          }}
        >
          {STRINGS.start}
        </Button>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safe: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },

  title: {
    fontSize: 36,
    fontWeight: "bold",
    margin: 15,
  },

  logo: {
    width: 300,
    height: 300,
  },

  instruction: {
    textAlign: "center",
    fontSize: 16,
    marginTop: 20,
    marginBottom: 5,
  },

  button: {
    minWidth: 200,
  },
});

export default Home;
