import React from "react";
import { StyleSheet, View, ActivityIndicator, TouchableOpacity, Text, ToastAndroid } from "react-native";
import { Button, Icon } from "@ui-kitten/components";
import CampusIoTForm from "./forms/CampusIoTForm";
import TTNForm from "./forms/TTNForm";
import HeliumForm from "./forms/HeliumForm";
import { getProfile } from "../api/CampusIoT";
import { connect } from "react-redux";
import Clipboard from '@react-native-community/clipboard';

import { STRINGS, NETWORKS } from "../assets/strings";

class NodeRegister extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      profiles: [],
      loading: true,
    };
    this.myFunction= "";
  }

  componentDidMount() {
    if (this.props.selectedNetwork === NETWORKS.campus_iot) {
      getProfile(this.props.jwt)
        .then((data) =>
          this.setState({ profiles: data.result, loading: false })
        )
        .catch((error) => alert(STRINGS.error + error));
    } else {
      this.setState({ loading: false });
    }
  }

  _display_form(profiles) {
    const api = this.props.selectedNetwork;
    if (api === NETWORKS.campus_iot) {
      return (
        <CampusIoTForm
          profiles={profiles}
          navigation={this.props.navigation}
          disabled={this.props.route.params.disabled}
        />
      );
    } else if (api === NETWORKS.ttn) {
      return (
        <TTNForm
          navigation={this.props.navigation}
          disabled={this.props.route.params.disabled}
        />
      );
    } else if (api === NETWORKS.helium) {
      return (
        <HeliumForm
          navigation={this.props.navigation}
          disabled={this.props.route.params.disabled}
        />
      );
    }
  }

  _confirmCopy(){
    ToastAndroid.show("Copied to clipboard !", 3);
    Clipboard.setString(this.myFunction)
  }

  render() {
    return (
      <View style={styles.main_view}>
        {!this.state.loading && this._display_form(this.state.profiles)}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  main_view: {
    flex: 1,
  },
  restart_view: {
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  form_view: {
    flex: 1,
  },
  button: {
    marginRight: "4%",
    marginTop: "2%",
  },
});

const mapStateToProps = (state) => {
  return {
    jwt: state.jwt,
    selectedNetwork: state.selectedNetwork,
  };
};

export default connect(mapStateToProps)(NodeRegister);
