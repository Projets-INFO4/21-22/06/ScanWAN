import React from "react";
import { SafeAreaView, View, FlatList } from "react-native";
import { connect } from "react-redux";
import ApplicationItem from "./ApplicationItem.js";
import { getAppList as ChirpstackFetcher } from "../api/CampusIoT";
import { getAppList as TTNFetcher } from "../api/TTN";
import { getDevicesList as HeliumFetcher } from "../api/Helium";

import { STRINGS, NETWORKS } from "../assets/strings";

class ApplicationSelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = { applications: [] };
  }

  async componentDidMount() {
    let applications;
    let token = this.props.jwt;

    switch (this.props.selectedNetwork) {
      case NETWORKS.campus_iot:
        applications = ChirpstackFetcher(token)
          .then((data) => this.setState({ applications: data.result }))
          .catch((error) => alert(STRINGS.error + error));
        break;
      case NETWORKS.ttn:
        applications = TTNFetcher(token)
          .then((data) => {
            let tmp = data.applications;
            let app = [];
            tmp.map((obj) => {
              app.push(obj.ids);
            });
            this.setState({ applications: app });
          })
          .catch((error) => alert(STRINGS.error + error));
        break;
      case NETWORKS.helium:
        applications = HeliumFetcher(token)
          .then((data) => {
            let tmp = data;
            let app = [];
            tmp.map((obj) => {
              app.push({ name: obj.name });
            });
            this.setState({ applications: app });
          })
          .catch((error) => alert(STRINGS.error + error));
        break;
      default:
        break;
    }
  }

  render() {
    return (
      <SafeAreaView>
        <FlatList
          data={this.state.applications}
          renderItem={({ item }) => (
            <ApplicationItem
              application={item}
              navigator={this.props.navigation}
            />
          )}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={renderSeparator}
        />
      </SafeAreaView>
    );
  }
}

const renderSeparator = () => {
  return <View style={{ backgroundColor: "black", height: 1, margin: 10 }} />;
};

const mapStateToProps = (state) => {
  return {
    selectedNetwork: state.selectedNetwork,
    jwt: state.jwt,
  };
};

export default connect(mapStateToProps)(ApplicationSelect);
