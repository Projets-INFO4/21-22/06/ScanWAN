import React, { Component } from "react";
import { connect } from "react-redux";
import QRCodeScanner from "react-native-qrcode-scanner";
import { RNCamera } from "react-native-camera";
import { Text, Button } from "@ui-kitten/components";
import { View, StyleSheet, Alert } from "react-native";

import { NETWORKS, EVENTS, SCREENS, STRINGS } from "../assets/strings";

class Scanner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scan: true,
      ScanResult: false,
      result: null,
    };
  }

  _onSuccess = (elt) => {
    const ds = elt.data.split(":");
    this.setState({
      result: elt,
      scan: false,
      ScanResult: true,
    });
    if (ds.length !== 0 && ds[0] === STRINGS.lora_code) {
      this.props.dispatch({ type: EVENTS.add_node, value: elt.data });
      this.props.navigation.navigate(SCREENS.NodeRegister, { disabled: true });
    } else {
      Alert.alert(STRINGS.error_title, STRINGS.unable_read_qr);
    }
  };

  _manual = () => {
    this.setState({ scan: false });
    if (this.props.route.params.selectedNetwork === undefined) {
      Alert.alert(STRINGS.error_title, STRINGS.choose_network);
    } else if (
      this.props.route.params.selectedApp === undefined &&
      this.props.route.params.selectedNetwork != NETWORKS.helium
    ) {
      Alert.alert(STRINGS.error_title, STRINGS.choose_application);
    } else {
      this.props.navigation.navigate(SCREENS.NodeRegister, {
        disabled: false,
      });
    }
  };

  displayScanButton() {
    return (
      <View style={styles.button_view}>
        <Text>{STRINGS.press_button_scan}</Text>
        <Button
          onPress={() => {
            this.setState({ scan: true });
          }}
        >
          {STRINGS.scan}
        </Button>
      </View>
    );
  }

  render() {
    const scan = this.state.scan;
    return (
      <View style={styles.scan_view}>
        <Text style={styles.title}>{STRINGS.title_scanner}</Text>
        {scan ? (
          <QRCodeScanner
            reactivate={true}
            reactivateTimeout={2500}
            onRead={this._onSuccess}
            flasMode={RNCamera.Constants.FlashMode.auto}
            topContent={<Text style={styles.textBold}>{STRINGS.scan_qr}</Text>}
            bottomContent={
              <Button
                onPress={() => {
                  this._manual();
                }}
              >
                {STRINGS.add_manually}
              </Button>
            }
          />
        ) : (
          this.displayScanButton()
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scan_view: {
    flex: 1,
  },

  title: {
    fontSize: 36,
    fontWeight: "bold",
    textAlign: "center",
    borderBottomColor: "black",
    borderBottomWidth: 1,
  },

  button_view: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => {
      dispatch(action);
    },
  };
};

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Scanner);
