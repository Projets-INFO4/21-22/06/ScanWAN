# PROJECT 6 : MOBILE APPLICATION REGISTERING IoT NODES

The LoRaSCAN application, available on Android and iOS, reads LoRaWAN QR codes and automatically registers them on LoRaWAN network operators, following the *TR005 LoRaWAN® Device Identification QR Codes specification*.

## Documentation

- [Documentation repository](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/06/docs)
- Readme of the project: [README.md](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/06/docs/-/blob/main/README.md)
- Tracking sheet of the project: [TRACKING_SHEET.md](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/06/docs/-/blob/main/TRACKING_SHEET.md)
- Mid-term presentation: [MID-TERM_PRESENTATION.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/06/docs/-/blob/main/ScanWAN_-_Pr%C3%A9sentation_mi-parcours.pdf)
- Final presentation : [LoRa_Scan_Présentation](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/06/docs/-/blob/main/LoRa_Scan_Pr%C3%A9sentation.pdf)
- Final report: [LoRa_Scan_Report](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/06/docs/-/blob/main/LoRa_Scan_Report.pdf)
