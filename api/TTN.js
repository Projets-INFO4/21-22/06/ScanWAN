import { Alert } from "react-native";

import { STRINGS, API } from "../assets/strings";

export async function getAppList(APIKey) {
  const url = API.ttn_applications;
  const headers = new Headers();
  headers.append("Centent-Type", "application/json");
  headers.append("Accept", "application/json");
  headers.append("Authorization", "Bearer " + APIKey);

  const appList = await fetch(url, {
    method: "GET",
    headers: headers,
  })
    .then((response) => {
      console.log(response);
      response.json();
    })
    .then((result) => {
      console.log(result);
      return result;
    })
    .catch((error) => {
      Alert.alert(STRINGS.error_title, STRINGS.no_applications);
    });
  return appList;
}

// Pattern name for ttnform
export async function addDevice(contentDevice, APIKey) {
  const middleurl = contentDevice.end_device.ids.application_ids.application_id;
  const url = API.ttn_applications + "/" + middleurl + API.ttn_devices;

  const headers = new Headers();
  headers.append("Content-Type", "application/json");
  headers.append("Authorization", "Bearer " + APIKey);

  const content = JSON.stringify(contentDevice);

  const res = await fetch(url, {
    method: "POST",
    headers: headers,
    body: content,
  })
    .then((response) => response.json())
    .then((result) => {
      if (result.code === 6) {
        Alert.alert(STRINGS.error_title, STRINGS.device_already_registered);
        return result.code;
      }
      return 0;
    })
    .catch((error) => {
      Alert.alert(STRINGS.error_title, STRINGS.issue_occured + error);
      return -1;
    });

  return res;
}
