import { Alert } from "react-native";

import { STRINGS, API } from "../assets/strings";

export async function getToken(id, pwd) {
  const url = API.campus_iot_login;
  const content = JSON.stringify({ username: id, password: pwd });
  const headers = new Headers();

  headers.append("Content-Type", "application/json");

  const token = await fetch(url, {
    method: "POST",
    body: content,
    headers: headers,
  })
    .then((response) => response.json())
    .then((result) => {
      if (result.code === 16) {
        Alert.alert(STRINGS.error_title, STRINGS.username_password_wrong);
        return -1;
      }
      return result.jwt;
    })
    .catch((error) => {
      Alert.alert(STRINGS.error_title, STRINGS.issue_occured + error);
    });

  return token;
}
export async function getNumberOfApp(token) {
  const url = API.campus_iot_applications;
  const headers = new Headers();
  headers.append("Content-Type", "application/json");
  headers.append("Grpc-Metadata-Authorization", "Bearer " + token);

  const apps = await fetch(url, {
    method: "GET",
    headers: headers,
  })
    .then((response) => response.json())
    .then((result) => {
      if (result.code === 16) {
        Alert.alert(STRINGS.error_title, STRINGS.session_expired);
        return -1;
      }
      return result.totalCount;
    })
    .catch((error) => {
      Alert.alert(STRINGS.error_title, API.issue_occured + error);
    });
  return apps;
}

export async function getAppList(token) {
  const url = API.campus_iot_applications;
  const limit = await getNumberOfApp(token);

  const headers = new Headers();
  headers.append("Content-Type", "application/json");
  headers.append("Accept", "application/json");
  headers.append("Grpc-Metadata-Authorization", "Bearer " + token);
  const param = "limit=" + limit;

  const appList = await fetch(url + "?" + param, {
    method: "GET",
    headers: headers,
  })
    .then((response) => response.json())
    .then((result) => {
      if (result.code === 16) {
        Alert.alert(STRINGS.error_title, STRINGS.session_expired);
        return -1;
      }
      return result;
    })
    .catch((error) => {
      Alert.alert(STRINGS.error_title, STRINGS.issue_occured + error);
    });

  return appList;
}

export async function getNumberOfProfile(token) {
  const url = API.campus_iot_device_profiles;
  const headers = new Headers();
  headers.append("Content-Type", "application/json");
  headers.append("Accept", "application/json");
  headers.append("Grpc-Metadata-Authorization", "Bearer " + token);

  const profileNumber = await fetch(url, {
    method: "GET",
    headers: headers,
  })
    .then((response) => response.json())
    .then((result) => {
      if (result.code === 16) {
        Alert.alert(STRINGS.error_title, STRINGS.session_expired);
        return -1;
      }
      return result.totalCount;
    })
    .catch((error) => {
      Alert.alert(STRINGS.error_title, STRINGS.issue_occured + error);
    });

  return profileNumber;
}
export async function getProfile(token) {
  const url = API.campus_iot_device_profiles;
  const limit = await getNumberOfProfile(token);

  const headers = new Headers();
  headers.append("Content-Type", "application/json");
  headers.append("Accept", "application/json");
  headers.append("Grpc-Metadata-Authorization", "Bearer " + token);
  const param = "limit=" + limit;

  const profileList = await fetch(url + "?" + param, {
    method: "GET",
    headers: headers,
  })
    .then((response) => response.json())
    .then((result) => {
      if (result.code === 16) {
        Alert.alert(STRINGS.error_title, STRINGS.session_expired);
        return -1;
      }
      return result;
    })
    .catch((error) => {
      Alert.alert(STRINGS.error_title, STRINGS.issue_occured + error);
    });

  return profileList;
}

export async function addDevice(deviceContent, token) {
  const url = API.campus_iot_devices;
  const headers = new Headers();
  headers.append("Content-Type", "application/json");
  headers.append("Grpc-Metadata-Authorization", "Bearer " + token);
  const content = JSON.stringify({ device: { ...deviceContent } });

  const res = await fetch(url, {
    method: "POST",
    headers: headers,
    body: content,
  })
    .then((response) => response.json())
    .then((result) => {
      if (result.code === 16) {
        Alert.alert(STRINGS.error_title, STRINGS.session_expired);
        return -1;
      } else if (result.code === 6) {
        Alert.alert(STRINGS.error_title, STRINGS.device_already_registered);
        return -1;
      }
      return 0;
    })
    .catch((error) => {
      Alert.alert(STRINGS.error_title, STRINGS.issue_occured + error);
    });

  return res;
}
