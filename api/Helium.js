import { Alert } from "react-native";

import { STRINGS, API } from "../assets/strings";

export async function getDevicesList(APIKey) {
  const url = API.helium_devices;
  const headers = new Headers();
  headers.append("key", APIKey);

  const appList = await fetch(url, {
    method: "GET",
    headers: headers,
  })
    .then((response) => response.json())
    .then((result) => {
      return result;
    })
    .catch((error) => {
      Alert.alert(STRINGS.error_title, STRINGS.no_applications);
    });
  return appList;
}

export async function addDevice(contentDevice, APIKey) {
  const url = API.helium_devices;
  const headers = new Headers();
  headers.append("key", APIKey);
  headers.append("content-type", "application/json");

  const content = JSON.stringify(contentDevice);

  const res = await fetch(url, {
    method: "POST",
    headers: headers,
    body: content,
  })
    .then((result) => {
      if (result.status != 201) {
        if (result.status === 422) {
          Alert.alert(STRINGS.error_title, STRINGS.device_already_registered);
        } else if (result.status === 404) {
          Alert.alert(
            STRINGS.error_title,
            result.status + ": " + STRINGS.missing_information
          );
        } else {
          Alert.alert(
            STRINGS.error_title,
            result.status + ": " + STRINGS.wrong_result_code
          );
        }
        return -1;
      }

      return 0;
    })
    .catch((error) => {
      Alert.alert(STRINGS.error_title, STRINGS.issue_occured + error);
      return -1;
    });

  return res;
}
