import { NativeModules } from "react-native";

/*
Strings displayed on screen
Only these should be translated
*/
const EN = {
  // Home
  welcome: "Register your LoRa devices now!",
  start: "Start",

  // Network Select
  select_network: "Select a network to register your device in",
  add_account: "Add a new network account",
  campus_iot: "Campus IoT",
  ttn: "TTN",
  helium: "Helium",

  // Scanner
  press_button_scan: "Press the button to scan a QR code",
  scan_qr: "Scan a LoRa QR code",
  scan: "Scan",
  add_manually: "Add manually",

  // Functions
  copied_clipboard: "Copied to clipboard!",
  select_encoding_matching_device: "Select the encoding function that matches your device",
  select_vendor: "Select a vendor...",
  select_device: "Select a device...",
  select_version: "Select a version...",
  select_profile: "Select a profile...",
  button_display_function: "Fonction",
  button_finish: "Finish",
  loading_vendors: "Loading vendors...",

  // Forms
  sucess_title: "Success",
  device_correctly_added: "The device has correctly been added",
  new_device: "A new device",
  device_name: "Name",
  description: "Description",
  dev_eui: "DevEUI",
  profile_id: "Profile ID",
  app_eui: "AppEUI",
  app_key: "App key",
  submit: "Submit",
  accept_ca: "I accept all certificates of authority (CA)",

  // Logins
  network_added: "Network added",
  login: "Login",
  login_needed: "You need to enter a login",
  password: "Password",
  password_needed: "You need to enter a password",
  api_key: "API key",
  api_key_needed: "You need to enter an API key",

  // Titles
  title_select_network: "Network selection",
  title_scanner: "Scanner",
  title_function_selection: "Encoding function selection",
  title_ttn_login: "TTN login",
  title_campus_iot_login: "CampusIoT login",
  title_helium_login: "Helium login",

  // Errors
  error_title: "Error", // title of Alerts.alert()
  error: "Error:\n",
  issue_occured: "Sorry, an issue occured:\n",
  unable_read_qr: "Unable to read the QR code",
  choose_network: "Please choose a network",
  choose_application: "Please choose an application",
  could_not_register_info: "Couldn't register your information",
  username_password_wrong: "Wrong username or password, please try again",
  session_expired: "Session expired, please log back in",
  device_already_registered: "The device is already registered on this network",
  no_applications: "No applications",
  wrong_result_code: "Wrong result code:\n",
  set_default_value: "Set default value for the form",
  name_restrictions:
    "The name can only contain letters, number and dashes, and must be at least 3 characters long",

  // Miscelaneaous
  lora_code: "LW",
};

const FR = {
  // Accueil
  welcome: "Enregistrez vos appareils LoRa maintenant !",
  start: "Commencer",

  // Sélection du réseau
  select_network: "Sélectionnez un réseau où enregistrer votre nœud",
  add_account: "Ajouter un nouveau compte de réseau",
  campus_iot: "Campus IoT",
  ttn: "TTN",
  helium: "Helium",

  // Scanner
  press_button_scan: "Appuyez sur le bouton pour scanner un QR code",
  scan_qr: "Scannez un QR code LoRa",
  scan: "Scan",
  add_manually: "Ajouter manuellement",

  // Fonctions
  copied_clipboard: "Copié dans le presse-papier !",
  select_encoding_matching_device: "Sélectionnez la fonction d'encodage correspondant à votre appareil",
  select_vendor: "Sélectionnez un constructeur...",
  select_device: "Sélectionnez un appareil...",
  select_version: "Sélectionnez une version...",
  select_profile: "Sélectionnez un profil régional...",
  button_display_function: "Fonction",
  button_finish: "Fin",
  loading_vendors: "Chargement des constructeurs...",

  // Formulaires
  sucess_title: "Succès",
  device_correctly_added: "L'appareil a bien été ajouté",
  new_device: "Un nouvel appareil",
  device_name: "Nom",
  description: "Description",
  dev_eui: "DevEUI",
  profile_id: "Profile ID",
  app_eui: "AppEUI",
  app_key: "App key",
  submit: "Soumettre",
  accept_ca: "J'accepte tous les certificats d'autorité (CA)",

  // Logins
  network_added: "Réseau ajouté",
  login: "Enregistrement",
  login_needed: "Vous devez entrer un login",
  password: "Mot de passe",
  password_needed: "Vous devez entrer un mot de passe",
  api_key: "Clé d'API",
  api_key_needed: "Vous devez entrer une clé d'API",

  // Titres
  title_select_network: "Sélection du réseau",
  title_scanner: "Scanner",
  title_function_selection: "Sélection de la fonction d'encodage",
  title_ttn_login: "Enregistrement TTN",
  title_campus_iot_login: "Enregistrement CampusIoT",
  title_helium_login: "Enregistrement Helium",

  // Erreurs
  error_title: "Erreur", // titre de Alerts.alert()
  error: "Erreur :\n",
  issue_occured: "Désolé, une erreur s'est produite :\n",
  unable_read_qr: "Impossible de lire le QR code",
  choose_network: "Veuillez choisir un réseau",
  choose_application: "Veuillez choisir une application",
  could_not_register_info: "Impossible d'enregistrer vos informations",
  username_password_wrong: "Mauvais login ou mot de passe, veuillez réessayer",
  session_expired: "Session expirée, veuillez vous reconnecter",
  device_already_registered: "L'appareil est déjà enregistré sur ce réseau",
  no_applications: "Pas d'applications",
  wrong_result_code: "Mauvais code de résultat :\n",
  set_default_value: "Définissez des valeurs pas défaut pour le formulaire",
  name_restrictions:
    "Le nom ne peut contenir que des lettres, chiffres et tirets bas, et dois faire au moins 3 caractères",
  missing_information: "Missing information",

  // Divers
  lora_code: "LW",
};

/*
Screens names
*/
const SCREENS = {
  Home: "Home",
  NetworkSelect: "NetworkSelect",
  NetworkItem: "NetworkItem",
  ApplicationSelect: "ApplicationSelect",
  ApplicationItem: "ApplicationItem",
  NodeRegister: "NodeRegister",
  FunctionSelect: "FunctionSelect",
  Scanner: "Scanner",
  CampusIoTLogin: "CampusIoTLogin",
  TTNLogin: "TTNLogin",
  HeliumLogin: "HeliumLogin",
};

/*
Networks names
*/
const NETWORKS = {
  campus_iot: "Campus IoT",
  ttn: "The Things Network",
  helium: "Helium",
};

/*
Dispatches names
*/
const EVENTS = {
  // Dispatch
  choose_network: "CHOOSE_NETWORK",
  choose_app: "CHOOSE_APP",
  add_jwt: "ADD_JWT",
  add_log: "ADD_LOG",
  add_node: "ADD_NODE",
  remove_app: "REMOVE_APP",
  clear: "CLEAR",

  // Events
  selected_network: "event.SelectedN",
  selected_application: "event.SelectedA",
  set_scan: "event.setScan",
  e_clear: "event.clear",
};

/*
Storage files
*/
const FILES = {
  // Filenames
  filename_campus_iot: "/campus_iot.json",
  filename_ttn: "/ttn.json",
  filename_helium: "/helium.json",
};

/*
API calls
Base URLs are used to construct call URLs
*/
const URL_CAMPUS_IOT = "https://lns.campusiot.imag.fr/api/";
const URL_HELIUM = "https://console.helium.com/api/v1/";
const URL_TTN = "https://eu1.cloud.thethings.network/api/v3/";
const API = {
  // Campus IoT
  campus_iot_login: URL_CAMPUS_IOT + "internal/login",
  campus_iot_applications: URL_CAMPUS_IOT + "applications",
  campus_iot_device_profiles: URL_CAMPUS_IOT + "device-profiles",
  campus_iot_devices: URL_CAMPUS_IOT + "devices",

  // TTN
  ttn_applications: URL_TTN + "applications",
  ttn_devices: "/devices",

  // Helium
  helium_devices: URL_HELIUM + "devices",

  // Functions
  functions_base_url: "https://raw.githubusercontent.com/TheThingsNetwork/lorawan-devices/master/vendor",
  functions_tree_url: "https://api.github.com/repos/TheThingsNetwork/lorawan-devices/git/trees/d579ec149a2cef4cd65be6a2da41f3ed61f8de2d?recursive=true",
  functions_index_path: "index.yaml",
};

const deviceLanguage =
  Platform.OS === "ios"
    ? NativeModules.SettingsManager.settings.AppleLocale ||
      NativeModules.SettingsManager.settings.AppleLanguages[0] //iOS 13
    : NativeModules.I18nManager.localeIdentifier;

var STRINGS = {};
switch (deviceLanguage.substring(0, 2)) {
  case "en":
    STRINGS = EN;
    break;
  case "fr":
    STRINGS = FR;
    break;
  default:
    STRINGS = EN;
    break;
}

export { STRINGS, SCREENS, NETWORKS, EVENTS, FILES, API };
