import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Home from "../components/Home";
import NetworkSelect from "../components/NetworkSelect";
import NetworkItem from "../components/NetworkItem";
import ApplicationSelect from "../components/ApplicationSelect";
import ApplicationItem from "../components/ApplicationItem";
import NodeRegister from "../components/NodeRegister";
import Scanner from "../components/Scanner";
import CampusIoTLogin from "../components/forms/CampusIoTLogin";
import TTNLogin from "../components/forms/TTNLogin";
import HeliumLogin from "../components/forms/HeliumLogin";

const Stack = createStackNavigator();

export default function MainNav() {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home"
        component={Home}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="NetworkSelect"
        component={NetworkSelect}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="NetworkItem"
        component={NetworkItem}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ApplicationSelect"
        component={ApplicationSelect}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ApplicationItem"
        component={ApplicationItem}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="NodeRegister"
        component={NodeRegister}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Scanner"
        component={Scanner}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="CampusIoTLogin"
        component={CampusIoTLogin}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="TTNLogin"
        component={TTNLogin}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="HeliumLogin"
        component={HeliumLogin}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}
